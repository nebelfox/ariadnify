from ariadnify.utils import File

DB_FILE_TEMPLATE = """
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import MetaData

convention = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}

metadata = MetaData(naming_convention=convention)

db = SQLAlchemy(metadata=metadata)


class Model(db.Model):
    __abstract__ = True

    def add(self):
        db.session.add(self)
        return self

    def commit(self):
        db.session.commit()
        return self

    def rollback(self):
        db.session.rollback()
        return self

    def save(self):
        return self.add().commit()

    def delete(self):
        db.session.delete(self)
        return self.commit()
""".lstrip()


def make_db_file() -> File:
    return File('db.py', DB_FILE_TEMPLATE)
