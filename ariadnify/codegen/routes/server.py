from ariadnify.utils import File

SERVER_ROUTE_FILE_TEMPLATE = """
from ariadne import graphql_sync
from flask import Flask, request, jsonify
from graphql import GraphQLSchema


def register_graphql_route(app: Flask, schema: GraphQLSchema) -> None:
    @app.route("/graphql", methods=["POST"])
    def graphql_server():
        data = request.get_json()
        success, result = graphql_sync(schema,
                                       data,
                                       context_value=request)
        return jsonify(result), (success and 200 or 400)
""".lstrip()


def make_server_route_file() -> File:
    return File('server.py', SERVER_ROUTE_FILE_TEMPLATE)
