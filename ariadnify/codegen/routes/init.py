from ariadnify.utils import File

ROUTES_INIT_FILE_TEMPLATE = """
from .explorer import register_explorer_route
from .server import register_graphql_route
from .healthcheck import register_healthcheck_route
""".lstrip()


def make_routes_init_file() -> File:
    return File('__init__.py', ROUTES_INIT_FILE_TEMPLATE)
