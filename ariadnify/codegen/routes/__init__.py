from ariadnify.utils import Package
from .explorer import make_explorer_route_file
from .healthcheck import make_healthcheck_route_file
from .server import make_server_route_file
from .init import make_routes_init_file


def make_routes_package() -> Package:
    return Package('routes', [
        make_routes_init_file(),
        make_server_route_file(),
        make_explorer_route_file(),
        make_healthcheck_route_file()
    ])
