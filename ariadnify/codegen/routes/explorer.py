from ariadnify.utils import File

EXPLORER_ROUTE_FILE_TEMPLATE = """
from flask import Flask

from gql import explorer_html


def register_explorer_route(app: Flask) -> None:
    @app.route("/graphql", methods=["GET"])
    def graphql_explorer():
        return explorer_html
""".lstrip()


def make_explorer_route_file() -> File:
    return File('explorer.py', EXPLORER_ROUTE_FILE_TEMPLATE)
