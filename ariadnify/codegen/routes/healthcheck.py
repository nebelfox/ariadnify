from ariadnify.utils import File

HEALTHCHECK_ROUTE_FILE_TEMPLATE = """
from flask import Flask


def register_healthcheck_route(app: Flask) -> None:
    @app.route("/healthcheck", methods=['GET'])
    def healthcheck():
        return 'Ok'
""".lstrip()


def make_healthcheck_route_file() -> File:
    return File('healthcheck.py', HEALTHCHECK_ROUTE_FILE_TEMPLATE)
