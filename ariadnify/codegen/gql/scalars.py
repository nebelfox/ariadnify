from typing import Iterable

from inflection import underscore as _

from ariadnify.schema import Scalar
from ariadnify.utils import Package, File, strip_margin, indent


def make_scalars_package(scalars: Iterable[Scalar]) -> Package:
    return Package('scalars', [
        make_scalars_init_file(scalars),
        *list(map(make_scalar_file, scalars))
    ])


def make_scalar_file(scalar: Scalar) -> File:
    name = _(scalar.name)
    return File(f'{name}.py', strip_margin(
        f"""from ariadne import ScalarType
           |
           |{name}_scalar = ScalarType('{scalar.name}')
           |
           |
           |@{name}_scalar.value_parser
           |def parse_{name}(value: None) -> None:
           |    pass
           |
           |
           |@{name}_scalar.serializer
           |def serialize_{name}(value: None) -> None:
           |    pass
           |"""))


def make_scalars_init_file(scalars: Iterable[Scalar]) -> File:
    imports = '\n'.join(f'from .{_(scalar.name)} import {_(scalar.name)}_scalar'
                        for scalar in scalars)
    scalars_seq = indent('\n'.join(f'{_(scalar.name)}_scalar,'
                                   for scalar in scalars))
    return File('__init__.py', strip_margin(
        f"""{imports}
           |
           |
           |all_scalars = (
           |    {scalars_seq}
           |)
           |"""))
