from inflection import underscore as _

from ariadnify.schema import Field, Object
from ariadnify.utils import Package, File, strip_margin, make_resolver_signature


def make_queries_package(query: Object) -> Package:
    return Package('queries', [
        make_queries_init_file(query),
        make_query_file(),
        *[File(f'{_(field.name)}.py', make_query_field_resolver(field))
          for field in query.fields if field.crud_operation]
    ])


def make_queries_init_file(query: Object) -> File:
    imports = '\n'.join(
        f'from .{_(f.name)} import resolve_{_(f.name)}'
        for f in query.fields)
    return File('__init__.py', strip_margin(
        f"""from .query import query
           |{imports}
           |"""))


def make_query_file() -> File:
    return File('query.py', strip_margin(
        """from typing import Any
          |
          |from ariadne import QueryType
          |from graphql import GraphQLResolveInfo
          |
          |query = QueryType()
          |"""))


def make_get_all_resolver(query_field: Field) -> str:
    model = query_field.type.type.name
    return_type = f'Iterable[{model}]'
    return strip_margin(
        f"""from typing import Any, Iterable
           |
           |from graphql import GraphQLResolveInfo
           |
           |from . import query
           |from models import {model}
           |
           |
           |@query.field('{query_field.name}')
           |{make_resolver_signature(query_field.name, return_type)}
           |    return {model}.query.all()
           |""")


def make_get_by_pk_resolver(query_field: Field) -> str:
    model = query_field.type.type.name
    return strip_margin(
        f"""from typing import Any
           |
           |from graphql import GraphQLResolveInfo
           |
           |from . import query
           |from models import {model}
           |
           |
           |@query.field('{query_field.name}')
           |{make_resolver_signature(query_field.name, model)}
           |    return {model}.query.get(tuple(kwargs.values()))
           |""")


def make_query_field_resolver(field: Field) -> str:
    match field.crud_operation:
        case 'GET_ALL':
            return make_get_all_resolver(field)
        case 'GET_BY_PK':
            return make_get_by_pk_resolver(field)
