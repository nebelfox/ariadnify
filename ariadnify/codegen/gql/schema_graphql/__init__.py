from ariadnify.schema import Schema
from ariadnify.utils import File
from .mutation import make_mutations_for_schema
from .query import make_queries_for_schema


def complete_schema(schema,
                    create_mutation_prefix: str,
                    update_mutation_prefix: str,
                    delete_mutation_prefix: str) -> Schema:
    schema.add_queries(*make_queries_for_schema(schema))
    schema.add_mutations(*make_mutations_for_schema(schema,
                                                    create_mutation_prefix,
                                                    update_mutation_prefix,
                                                    delete_mutation_prefix))
    return schema


def make_schema_graphql_file(schema: Schema,
                             create_mutation_prefix: str,
                             update_mutation_prefix: str,
                             delete_mutation_prefix: str) -> File:
    return File('schema.graphql', str(complete_schema(schema,
                                                      create_mutation_prefix,
                                                      update_mutation_prefix,
                                                      delete_mutation_prefix)))
