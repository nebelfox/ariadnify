from typing import List

from inflection import camelize, pluralize

from ariadnify.schema import (Object,
                              Field,
                              Arg,
                              TypeSpec,
                              VALUE,
                              CONTAINS,
                              RANGE,
                              TIME_RANGE,
                              Schema,
                              Description)

filter_name_suffixes = {
    VALUE: ('',),
    CONTAINS: ('Contains',),
    RANGE: ('GreaterThan', 'LessThan'),
    TIME_RANGE: ('After', 'Before')
}


def field_to_filter_args(field: Field) -> List[Arg]:
    return [
        Arg(name=f'{field.filter.make_field_name(field.name)}{suffix}',
            description=Description.empty(),
            type=TypeSpec(field.type.type))
        for suffix in filter_name_suffixes[field.filter.kind]
    ]


def object_to_query_all(obj: Object) -> Field:
    return Field(name=camelize(pluralize(obj.name), False),
                 description=Description(
                     f'Get all the {camelize(pluralize(obj.name))}'),
                 type=TypeSpec(type=obj,
                               is_required=True,
                               is_array=True,
                               items_required=True),
                 args=[arg for f in obj.filterable_fields
                       for arg in field_to_filter_args(f)],
                 crud_operation='GET_ALL')


def humanize_seq(items: List[str],
                 sep: str = ', ',
                 last_sep: str = ' and ') -> str:
    return f'{sep.join(str(item) for item in items[:-1])}{last_sep * (len(items) > 1)}{items[-1]}'


def object_to_query_by_pk(obj: Object) -> Field:
    pk = obj.primary_key
    if not pk:
        return None
    args = [
        Arg(name=pk_field.name,
            description=pk_field.description,
            type=TypeSpec.required(pk_field.type.type))
        for pk_field in pk
    ]
    return Field(name=camelize(obj.name, False),
                 description=Description(
                     f'Get a specific {obj.name} by its '
                     f'{humanize_seq([pk_field.name for pk_field in pk])}'
                 ),
                 type=TypeSpec.required(obj),
                 args=args,
                 crud_operation='GET_BY_PK')


def make_queries_for_object(obj: Object) -> List[Field]:
    return list(filter(bool, (object_to_query_all(obj),
                              object_to_query_by_pk(obj))))


def make_queries_for_schema(schema: Schema) -> List[Field]:
    return sum((make_queries_for_object(obj)
                for obj in schema.objects), [])
