from typing import List

from inflection import camelize, singularize

from ariadnify.schema import (Object,
                              OBJECT,
                              Field,
                              Arg,
                              TypeSpec,
                              Schema,
                              Description)


def field_to_args(field: Field, required: bool = None) -> List[Arg]:
    is_required = field.type.is_required if required is None else required
    if field.type.type.kind == OBJECT:
        pk = field.type.type.primary_key

        return [
            Arg(name=(f'{singularize(field.name)}'
                      f'{camelize(pk_field.name)}'
                      f'{"s" * field.type.is_array}'),
                ref_type=field.type,
                description=Description.empty(),
                type=TypeSpec(type=pk_field.type.type,
                              is_required=is_required,
                              is_array=field.type.is_array,
                              items_required=field.type.items_required))
            for pk_field in pk
        ]
    return [Arg(name=field.name,
                description=Description.empty(),
                type=TypeSpec(type=field.type.type,
                              is_required=is_required,
                              is_array=field.type.is_array,
                              items_required=field.type.items_required))]


def field_to_edit_args(field: Field) -> List[Arg]:
    return field_to_args(field, required=field.is_pk)


def object_to_mutation_create(obj: Object, name_prefix: str) -> Field:
    return Field(name=f'{name_prefix}{obj.name}',
                 description=Description.empty(),
                 type=TypeSpec.required(obj),
                 args=sum(map(field_to_args, obj.physical_attributes), []),
                 crud_operation='CREATE')


def object_to_mutation_update(obj: Object, name_prefix: str) -> Field:
    return Field(name=f'{name_prefix}{obj.name}',
                 description=Description.empty(),
                 type=TypeSpec.required(obj),
                 args=sum(map(field_to_edit_args, obj.physical_fields), []),
                 crud_operation='UPDATE')


def object_to_mutation_delete(obj: Object, name_prefix: str) -> Field:
    return Field(name=f'{name_prefix}{obj.name}',
                 description=Description.empty(),
                 type=TypeSpec.required(obj),
                 args=sum(map(field_to_edit_args, obj.primary_key), []),
                 crud_operation='DELETE')


def make_mutations_for_object(obj: Object,
                              create_prefix: str,
                              edit_prefix: str,
                              delete_prefix: str) -> List[Field]:
    return list(filter(bool, (object_to_mutation_create(obj, create_prefix),
                              object_to_mutation_update(obj, edit_prefix),
                              object_to_mutation_delete(obj, delete_prefix))))


def make_mutations_for_schema(schema: Schema,
                              create_name_prefix: str,
                              edit_prefix: str,
                              delete_prefix: str) -> List[Field]:
    return sum((make_mutations_for_object(obj, create_name_prefix, edit_prefix,
                                          delete_prefix)
                for obj in schema.objects), [])
