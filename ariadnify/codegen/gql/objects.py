from typing import Iterable

from inflection import underscore as _

from ariadnify.schema import Object, Field
from ariadnify.utils import Package, File, strip_margin, indent


def make_objects_package(objects: Iterable[Object]) -> Package:
    objects_with_virtual_fields = [obj for obj in objects
                                   if any(f.is_virtual for f in obj.fields)]
    return Package('objects', [
        make_objects_init_file(objects_with_virtual_fields),
        *list(filter(bool, map(make_object_file, objects_with_virtual_fields)))
    ])


def make_object_field_resolver(obj: Object, field: Field) -> str:
    args = ',\n'.join([f'{_(obj.name)}: {obj.name}',
                       'info: GraphQLResolveInfo']
                      + [arg.name for arg in field.args])
    prefix = f'def resolve_{_(obj.name)}_{_(field.name)}('
    n = len(prefix)
    return strip_margin(
        f"""@{_(obj.name)}_object.field('{field.name}')
           |{prefix}{indent(args, n)}) -> None:
           |    pass
           |""")


def make_object_file(obj: Object) -> File:
    resolvers = '\n\n'.join(make_object_field_resolver(obj, f)
                            for f in obj.fields if f.is_virtual)
    return File(f'{_(obj.name)}.py', strip_margin(
        f"""from graphql import GraphQLResolveInfo
           |from ariadne import ObjectType
           |
           |from models import {obj.name}
           |
           |{_(obj.name)}_object = ObjectType({obj.name}.__name__)
           |
           |
           |{resolvers}
           |"""))


def make_objects_init_file(objects: Iterable[Object]) -> File:
    imports = '\n'.join(f'from .{_(obj.name)} import {_(obj.name)}_object'
                        for obj in objects)
    objects_seq = indent('\n'.join(f'{_(obj.name)}_object,' for obj in objects))
    return File('__init__.py', strip_margin(
        f"""{imports}
           |
           |
           |all_objects = (
           |    {objects_seq}
           |)
           |"""))
