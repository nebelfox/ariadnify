from ariadnify.utils import File

EXPLORER_FILE_TEMPLATE = """
from ariadne.explorer import Explorer{kind}

explorer_html = Explorer{kind}().html(None)
""".lstrip()


def make_explorer_file(explorer_kind: str) -> File:
    return File('explorer.py',
                EXPLORER_FILE_TEMPLATE.format(kind=explorer_kind))
