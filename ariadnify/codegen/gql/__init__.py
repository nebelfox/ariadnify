from ariadnify.schema import Schema
from ariadnify.utils import Package
from .explorer import make_explorer_file
from .mutation import make_mutations_package
from .objects import make_objects_package
from .query import make_queries_package
from .scalars import make_scalars_package
from .schema import make_schema_file
from .schema_graphql import complete_schema, make_schema_graphql_file
from .init import make_gql_init_file


def make_gql_package(schema: Schema,
                     create_mutation_prefix: str,
                     update_mutation_prefix: str,
                     delete_mutation_prefix: str,
                     explorer_kind: str) -> Package:
    return Package('gql', [
        make_gql_init_file(),
        make_schema_graphql_file(schema,
                                 create_mutation_prefix,
                                 update_mutation_prefix,
                                 delete_mutation_prefix),
        make_schema_file(),
        make_explorer_file(explorer_kind),
        make_scalars_package(schema.scalars),
        make_objects_package(schema.objects),
        make_queries_package(schema.query),
        make_mutations_package(schema.mutation)
    ])
