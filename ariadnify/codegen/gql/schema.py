from ariadnify.utils import File

SCHEMA_FILE_TEMPLATE = """
from ariadne import (load_schema_from_path,
                     make_executable_schema as _make_executable_schema)
from graphql import GraphQLSchema

from gql import (query,
                 mutation,
                 all_scalars,
                 all_objects)


def make_executable_schema() -> GraphQLSchema:
    return _make_executable_schema(load_schema_from_path('gql/schema.graphql'),
                                   query,
                                   mutation,
                                   *all_scalars,
                                   *all_objects,
                                   convert_names_case=True)
""".lstrip()


def make_schema_file() -> File:
    return File('schema.py', SCHEMA_FILE_TEMPLATE)
