from inflection import underscore as _, pluralize as plr

from ariadnify.schema import Field, Object
from ariadnify.utils import (Package,
                             File,
                             strip_margin,
                             indent,
                             make_resolver_signature)


def make_mutations_package(query: Object) -> Package:
    return Package('mutations', [
        make_mutations_init_file(query),
        make_mutation_file(),
        *[File(f'{_(field.name)}.py', make_mutation_field_resolver(field))
          for field in query.fields if field.crud_operation]
    ])


def make_mutations_init_file(query: Object) -> File:
    imports = '\n'.join(
        f'from .{_(f.name)} import resolve_{_(f.name)}'
        for f in query.fields)
    return File('__init__.py', strip_margin(
        f"""from .mutation import mutation
           |{imports}
           |"""))


def make_mutation_file() -> File:
    return File('mutation.py', strip_margin(
        """from typing import Any
          |
          |from graphql import GraphQLResolveInfo
          |
          |from ariadne import MutationType
          |
          |mutation = MutationType()
          |"""))


def make_create_resolver_body(field: Field) -> str:
    model = field.type.type.name
    refs = [arg for arg in field.args if arg.ref_type is not None]
    ref_kwargs = [
        (f'{_(ref.name)}=[{model}.get(pk) for pk in kwargs[\'{_(ref.name)}\']]'
         if ref.type.is_array
         else f'{_(ref.name)}={model}.get({_(ref.name)})')
        for ref in refs
    ]
    if ref_kwargs:
        kwargs = ',\n'.join(ref_kwargs + [
            f'**{{k:v for k, v in kwargs.items()\n'
            f'   if k not in {[a.name for a in refs]!r}}}).save()'
        ])
        prefix = f'return {model}('
        n = len(prefix)
        return f'{prefix}{indent(kwargs, n)}'
    return f'return {model}(**kwargs).save()'


def make_update_resolver_body(field: Field) -> str:
    model = field.type.type.name
    instance = _(model)
    ref_args = [arg for arg in field.args if arg.ref_type is not None]

    ref_assigns = [
        f'{instance}.{_(plr(arg.ref_type.type.name))} = [{arg.ref_type.type.name}.get(pk) for pk in kwargs[\'{_(arg.name)}\']]'
        if arg.ref_type.is_array
        else f'{instance}.{_(arg.ref_type.type.name)} = {arg.ref_type.type.name}.get(kwargs[\'{_(arg.name)}\'])'
        for arg in ref_args
    ]
    scalar_assigns = [
        f'{instance}.{_(arg.name)} = kwargs[\'{_(arg.name)}\']'
        for arg in field.args
        if arg.ref_type is None
    ]
    assigns = '\n'.join(scalar_assigns + ref_assigns)
    return strip_margin(
        f"""if not ({instance} := {model}.query.get(tuple(kwargs.values()))):
           |    raise ValueError(f'{model} {{kwargs}} doesn\\'t exist')
           |
           |{assigns}
           |
           |return {instance}.save()""")


def make_delete_resolver_body(field: Field) -> str:
    model = field.type.type.name
    instance = _(model)
    return strip_margin(
        f"""if not ({instance} := {model}.query.get(tuple(kwargs.values()))):
           |    raise ValueError(f'{model} {{kwargs}} doesn\\'t exist')
           |return {instance}.delete()""")


def make_mutation_field_resolver_body(field: Field) -> str:
    match field.crud_operation:
        case 'CREATE':
            return make_create_resolver_body(field)
        case 'UPDATE':
            return make_update_resolver_body(field)
        case 'DELETE':
            return make_delete_resolver_body(field)


def make_mutation_field_resolver(field: Field) -> str:
    model = field.type.type.name
    ref_models = [
        arg.ref_type.type.name
        for arg in field.args
        if arg.ref_type is not None
    ]
    model_imports = ', '.join([model] + ref_models)
    return strip_margin(
        f"""from typing import Any
           |
           |from graphql import GraphQLResolveInfo
           |
           |from . import mutation
           |from models import {model_imports}
           |
           |
           |@mutation.field('{field.name}')
           |{make_resolver_signature(field.name, model)}
           |    {indent(make_mutation_field_resolver_body(field), 4)}
           |""")
