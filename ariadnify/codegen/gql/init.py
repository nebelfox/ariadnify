from ariadnify.utils import File

GQL_INIT_FILE_TEMPLATE = """
from .explorer import explorer_html
from .scalars import all_scalars
from .objects import all_objects
from .queries import query
from .mutations import mutation
from .schema import make_executable_schema
""".lstrip()


def make_gql_init_file() -> File:
    return File('__init__.py', GQL_INIT_FILE_TEMPLATE)
