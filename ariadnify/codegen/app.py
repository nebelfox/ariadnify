from ariadnify.utils import File

APP_FILE_TEMPLATE = """
from flask import Flask
from flask_migrate import Migrate

import config
from db import db
from gql import make_executable_schema
from routes import (register_graphql_route,
                    register_explorer_route,
                    register_healthcheck_route)


app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = config.DB_CONN_URL
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["JSON_SORT_KEYS"] = False

db.init_app(app)
migrate = Migrate(app, db)
schema = make_executable_schema()

register_graphql_route(app, schema)
register_explorer_route(app)
register_healthcheck_route(app)

if __name__ == '__main__':
    app.run(debug=True)
""".lstrip()


def make_app_file() -> File:
    return File('app.py', APP_FILE_TEMPLATE)
