from ariadnify.utils import File

CONFIG_FILE_TEMPLATE = r"""
from typing import Any
from os import getenv


def env(name: str, 
        default: Any = None, 
        value_type: type = str, 
        required: bool = True) -> Any:
    if not (value := getenv(name)):
        if required:
            raise ValueError(f'Required environment variable \'{name}\' '
                             f'is empty')
        return default
    return value_type(value)


DB_CONN_URL: str = env('DB_CONN_URL')
""".lstrip()


def make_config_file() -> File:
    return File('config.py', CONFIG_FILE_TEMPLATE)
