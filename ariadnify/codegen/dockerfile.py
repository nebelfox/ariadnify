from ariadnify.utils import File

DOCKERFILE_TEMPLATE = """
FROM {base_image}

RUN pip install pipenv

COPY Pipfile Pipfile.lock ./
RUN pipenv install --system --deploy --ignore-pipfile

COPY src/ app/
WORKDIR app

CMD ["gunicorn", "app:app", "--bind=0.0.0.0:5000"]
""".lstrip()


def make_dockerfile(base_image: str) -> File:
    return File('Dockerfile', DOCKERFILE_TEMPLATE.format(base_image=base_image))
