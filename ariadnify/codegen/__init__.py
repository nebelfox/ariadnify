from ariadnify.schema import Schema
from ariadnify.utils import Package
from .app import make_app_file
from .config import make_config_file
from .db import make_db_file
from .dockerfile import make_dockerfile
from .gql import make_gql_package, complete_schema
from .models import make_models_package
from .routes import make_routes_package


def make_src_package(schema: Schema,
                     create_mutation_prefix: str,
                     update_mutation_prefix: str,
                     delete_mutation_prefix: str,
                     explorer_kind: str) -> Package:
    return Package('src', [
        make_app_file(),
        make_config_file(),
        make_db_file(),
        make_routes_package(),
        make_models_package(schema),
        make_gql_package(schema,
                         create_mutation_prefix,
                         update_mutation_prefix,
                         delete_mutation_prefix,
                         explorer_kind)])
