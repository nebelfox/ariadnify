from collections import defaultdict
from operator import itemgetter
from typing import List, Iterable, Tuple, Set, Any, Callable
from itertools import chain

from inflection import underscore as _

from ariadnify.schema import Field, OBJECT, SCALAR, Object
from ariadnify.utils import indent
from . import make_sqla_type_import


def object_to_imports(obj: Object) -> Iterable[Tuple[str, str]]:
    yield 'sqlalchemy', 'Column'
    if obj.primary_key:
        yield 'sqlalchemy', 'PrimaryKeyConstraint'
    if any(f.is_unique for f in obj.fields):
        yield 'sqlalchemy', 'UniqueConstraint'
    if any(f.type.type.kind == OBJECT for f in obj.all_fields):
        yield 'sqlalchemy', 'ForeignKeyConstraint'
        yield 'sqlalchemy.orm', 'relationship'
    yield 'db', 'Model'
    yield from filter(bool, chain(*tuple(map(field_to_imports, obj.all_fields))))


def field_to_imports(field: Field) -> Iterable[Tuple[str, str]]:
    if field.type.type.kind == OBJECT and not field.type.is_array:
        yield f'.{_(field.type.type.name)}', field.type.type.name
        for pk_field in field.type.type.primary_key:
            yield make_sqla_type_import(pk_field.type.type.name)
    if field.type.type.kind == SCALAR:
        yield make_sqla_type_import(field.type.type.name)


def make_import(origin: str, targets: Set[str]) -> str:
    prefix = f'from {origin} import '
    if len(targets) == 1:
        return f'{prefix}{next(iter(targets))}'
    n = len(prefix) + 1
    suffix = ',\n'.join(targets)
    return f'{prefix}({indent(suffix, n)})'


class Groups(defaultdict):
    def __missing__(self, key: Any) -> list:
        self[key] = value = []
        return value


def group_by(iterable: Iterable[Any],
             key: Callable[[Any], Any]) -> Iterable[Tuple[Any, Iterable[Any]]]:
    groups = Groups()
    for item in iterable:
        groups[key(item)].append(item)
    return groups.items()


def merge_imports(imports: Iterable[Tuple[str, str]]) -> List[str]:
    return [
        make_import(origin, set(map(itemgetter(1), targets)))
        for origin, targets in group_by(imports, itemgetter(0))
    ]
