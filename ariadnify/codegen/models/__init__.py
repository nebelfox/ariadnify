from ariadnify.schema import Schema
from ariadnify.utils import Package
from .types import make_sqla_type_expr, make_sqla_type_import
from .constraints import make_table_args
from .imports import merge_imports, field_to_imports, object_to_imports
from .model import object_to_models, make_init_file


def make_models_package(schema: Schema) -> Package:
    models = sum(map(object_to_models, schema.objects), [])
    return Package('models', [
        make_init_file(models),
        *models
    ])
