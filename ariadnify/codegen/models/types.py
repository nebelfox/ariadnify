from typing import Tuple

types_mapping = {
    'String': {},
    'Int': {'class': 'Integer'},
    'Float': {'class': 'Numeric'},
    'Boolean': {},
    'ID': {'class': 'Integer'}
}


def make_sqla_type_expr(graphql_type: str) -> str:
    props = types_mapping[graphql_type]
    cls = props.get('class', graphql_type)
    args = ', '.join(map(str, props.get('args', ())))
    return f'{cls}({args})'


def make_sqla_type_import(graphql_type: str) -> Tuple[str, str]:
    props = types_mapping[graphql_type]
    return (props.get('import_from', 'sqlalchemy'),
            props.get('class', graphql_type))
