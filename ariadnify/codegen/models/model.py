from typing import List

from inflection import underscore as _, singularize as sng, pluralize as plr

from ariadnify.codegen.models import (make_table_args,
                                      merge_imports,
                                      make_sqla_type_expr,
                                      object_to_imports)
from ariadnify.schema import Field, Object, OBJECT
from ariadnify.utils import Model, File, indent, strip_margin

DEBUG = False


def make_backref(obj: Object, field: Field) -> str | None:
    if len(ref_fields := [f for f in field.type.type.fields if
                          f.type.type == obj]) > 1:
        raise ValueError()
    if len(ref_fields) < 1:
        return None
    return f'backref=\'{_(ref_fields[0].name)}\''


def get_ref(obj: Object,
            field: Field,
            raise_if_none: bool = True) -> Field | None:
    DEBUG and print(f'### get_ref({obj.name}, {field.name})')
    other = field.type.type
    backrefs: List[Field] = [f for f in other.all_fields if f.type.type == obj]
    if len(backrefs) > 1:
        raise ValueError(f'Multiple fields of type {other.name} '
                         f'point to type {obj.name}. Cannot determine '
                         f'the kind of relationship')
    if len(backrefs) < 1:
        if raise_if_none:
            raise ValueError(f'No corresponding field of type {obj.name} '
                             f'in type {other.name}. Could not determine '
                             f'the kind of relationship')
        return None
    return backrefs[0]


def make_to_one(field: Field, ref_field: Field) -> List[str]:
    DEBUG and print(f'### make_to_one({field.name}, {ref_field.name})')
    args = ', '.join(filter(bool, (
        field.type.type.name,
        f'backref=\'{_(ref_field.name)}\'',
        f'uselist={ref_field.type.is_array}',
        f'lazy=True'
    )))
    return [
        (f'{_(field.type.type.name)}_'
         f'{field_to_model_columns(field.type.type, pk_field)[0]}')
        for pk_field in field.type.type.primary_key
    ] + [f'{_(field.type.type.name)} = relationship({args})']


def make_secondary_relationship(table_name: str,
                                left: Field,
                                right: Field) -> str:
    args = ',\n'.join((f'\'{left.type.type.name}\'',
                       f'secondary=\'{table_name}\'',
                       f'back_populates=\'{_(plr(right.type.type.name))}\'',
                       'lazy=True'))
    prefix = f'{_(plr(left.type.type.name))} = relationship('
    n = len(prefix)
    return f'{prefix}{indent(args, n)})'


def field_to_ref(field: Field) -> List[str]:
    DEBUG and print(f'### field_to_ref({field.name})')
    ref_name = _(sng(field.name))
    ref_table = _(field.type.type.name)
    columns = [
        (f'{ref_name}_{pk_field.name}',
         make_sqla_type_expr(pk_field.type.type.name))
        for pk_field in field.type.type.primary_key]
    ref_columns = [f'{ref_table}.{pk_field.name}'
                   for pk_field in field.type.type.primary_key]
    return [
        f'Column(\'{column}\', {sql_type})'
        for column, sql_type in columns
    ] + [f'ForeignKeyConstraint({[c for c, t in columns]!r}, {ref_columns!r})']


def make_association_table_name(left: Field, right: Field) -> str:
    DEBUG and print(
        f'### make_association_table_name({left.name}, {right.name})')
    table_name = '_'.join(map(lambda t: _(t.name), sorted(
        (left.type.type, right.type.type), key=lambda t: t.index)))
    return table_name


def make_association_table(obj: Object, field: Field) -> Model | None:
    DEBUG and print(f'### make_association_table({obj.name}, {field.name})')
    if (field.type.type.kind == OBJECT
            and field.type.is_array
            and (ref_field := get_ref(obj, field, raise_if_none=False))
            and ref_field.type.is_array
            and ref_field.type.type.index < field.type.type.index):
        table_name = make_association_table_name(field, ref_field)
        args = ',\n'.join(field_to_ref(field) + field_to_ref(ref_field))
        return Model(
            model_name=table_name,
            name=f'{_(table_name)}.py',
            content=strip_margin(
                f"""from sqlalchemy import Table, Column, Integer, ForeignKeyConstraint 
                   |
                   |from db import db
                   |
                   |
                   |{table_name} = db.Table(
                   |    '{table_name}',
                   |    {indent(args)}
                   |)
                   |"""))


def make_many_to_many(left: Field, right: Field) -> List[str]:
    DEBUG and print(f'### make_many_to_many({left.name}, {right.name})')
    table_name = make_association_table_name(left, right)
    return [make_secondary_relationship(table_name, right, left)]


def make_relationship(obj: Object,
                      field: Field) -> List[str]:
    DEBUG and print(f'### make_relationship({obj.name}, {field.name})')
    ref_field = get_ref(obj, field)
    if not field.type.is_array:
        if ref_field.type.is_array or obj.index < field.type.type.index:
            return make_to_one(field, ref_field)
    if ref_field.type.is_array:
        return make_many_to_many(ref_field, field)
    return []


def field_to_model_columns(obj: Object,
                           field: Field) -> List[str]:
    DEBUG and print(f'### field_to_model_columns({obj.name}, {field.name})')
    if field.is_virtual:
        return []
    if field.type.type.kind == OBJECT:
        return make_relationship(obj, field)
    if sql_type := make_sqla_type_expr(field.type.type.name):
        return [
            f'{_(field.name)} = Column({sql_type}, '
            f'nullable={not field.type.is_required})']
    raise KeyError(f'No corresponding SQL type '
                   f'for GraphQL type {field.type.type.name}')


def object_to_models(obj: Object) -> List[Model]:
    DEBUG and print(f'### object_to_model({obj.name})')

    columns = '\n'.join(sum((field_to_model_columns(obj, field)
                             for field in obj.all_fields), []))
    association_tables = list(filter(bool, (make_association_table(obj, field)
                                            for field in obj.all_fields)))
    imports = '\n'.join(merge_imports(object_to_imports(obj)))
    model = Model(model_name=obj.name,
                  name=f'{_(obj.name)}.py',
                  content=strip_margin(
                      f"""{imports}
                         |
                         |
                         |class {obj.name}(Model):
                         |    {indent(columns)}
                         |    {indent(make_table_args(obj))}
                         |"""))
    return [model] + association_tables


def make_init_file(models: List[Model]) -> File:
    return File(name='__init__.py',
                content='\n'.join(
                    f'from .{_(model.model_name)} import {model.model_name}'
                    for model in models
                ) + '\n')
