from inflection import underscore as _, singularize as sng

from ariadnify.schema import Object, Field, OBJECT
from ariadnify.utils import indent, strip_margin


def make_primary_key_constraint(obj: Object) -> str:
    if pk := obj.primary_key:
        return (f'PrimaryKeyConstraint('
                f'{", ".join(pk_field.name for pk_field in pk)})')


def make_foreign_key_constraint(field: Field) -> str:
    if field.type.type.kind == OBJECT and not field.type.is_array:
        ref_name = _(sng(field.name))
        ref_model = field.type.type.name
        columns = [f'{ref_name}_{pk_field.name}'
                   for pk_field in field.type.type.primary_key]
        ref_columns = [f'{ref_model}.{pk_field.name}'
                       for pk_field in field.type.type.primary_key]
        return (f'ForeignKeyConstraint('
                f'[{", ".join(columns)}], '
                f'[{", ".join(ref_columns)}])')


def make_unique_constraint(field: Field) -> str:
    if field.is_unique:
        return f'UniqueConstraint({field.name})'


def make_table_args(obj: Object) -> str:
    args = ',\n'.join(filter(bool, (
        make_primary_key_constraint(obj),
        *list(map(make_unique_constraint, obj.fields)),
        *list(map(make_foreign_key_constraint, obj.all_fields))
    )))
    if args:
        return strip_margin(
            f"""
               |__table_args__ = (
               |    {indent(args)},
               |)""")
    return ''
