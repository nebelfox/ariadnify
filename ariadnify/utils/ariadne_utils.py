from inflection import underscore as _

from .string_utils import indent

RESOLVER_ARGS = """
parent: Any,
info: GraphQLResolveInfo,
**kwargs
""".strip()


def make_resolver_signature(field_name: str, return_type: str) -> str:
    prefix = f'def resolve_{_(field_name)}('
    n = len(prefix)
    return f'{prefix}{indent(RESOLVER_ARGS, n)}) -> {return_type}:'
