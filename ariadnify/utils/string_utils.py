from re import compile

MARGIN_PATTERN = compile('\n *\\|')


def strip_margin(text: str) -> str:
    return MARGIN_PATTERN.sub("\n", text)


def indent(text: str, n_spaces: int = 4) -> str:
    return text.replace("\n", "\n" + (" " * n_spaces))


def trim_suffix(s: str, suffix: str) -> str:
    if s.endswith(suffix):
        return s[:-len(suffix)]
    return s
