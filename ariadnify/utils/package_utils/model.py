from dataclasses import dataclass

from .file import File


@dataclass
class Model(File):
    model_name: str
