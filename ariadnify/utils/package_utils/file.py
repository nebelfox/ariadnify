from dataclasses import dataclass

from os.path import join


@dataclass
class File:
    name: str
    content: str

    def write(self, base_dir: str):
        with open(join(base_dir, self.name), 'w') as file:
            file.write(self.content)
