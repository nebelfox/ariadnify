from __future__ import annotations

from dataclasses import dataclass, field
from os import makedirs
from os.path import join
from typing import List, Dict

from .file import File


@dataclass
class Package:
    name: str
    items: List[File | Package] = field(default_factory=list)

    def write(self, base_dir: str) -> None:
        self_dir = join(base_dir, self.name)
        makedirs(self_dir, exist_ok=True)
        for item in self.items:
            item.write(self_dir)

    @staticmethod
    def from_dict(name: str, items: Dict[str, str]) -> Package:
        return Package(name, [File(file_name, content)
                              for file_name, content in items.items()])
