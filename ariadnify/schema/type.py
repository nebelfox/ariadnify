from dataclasses import dataclass, field
from typing import Literal

from ariadnify.schema import Node

SCALAR = 'scalar'
OBJECT = 'type'


@dataclass
class Type(Node):
    kind: Literal[SCALAR] | Literal[OBJECT] = field(init=False)

    def __str__(self):
        return f'{self.description}{self.kind} {self.name}'
