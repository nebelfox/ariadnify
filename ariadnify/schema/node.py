from dataclasses import dataclass

from graphql import DefinitionNode

from ariadnify.schema import Description


@dataclass
class Node:
    name: str
    description: Description

    @staticmethod
    def kwargs_from_node(node: DefinitionNode) -> dict:
        return dict(
            name=node.name.value,
            description=Description.from_string_value_node(node.description))
