from dataclasses import dataclass

from graphql import ScalarTypeDefinitionNode

from ariadnify.schema import Filter, SCALAR, Type


@dataclass
class Scalar(Type):
    kind = SCALAR
    filter: Filter = None

    @staticmethod
    def from_scalar_node(node: ScalarTypeDefinitionNode):
        return Scalar(**Type.kwargs_from_node(node),
                      filter=Filter.from_directives(node.directives))
