from dataclasses import dataclass

from graphql import TypeNode

from ariadnify.schema import Type


@dataclass
class TypeSpec:
    type: Type
    is_required: bool = False
    is_array: bool = False
    items_required: bool = False

    def __str__(self):
        return (f'{"[" * self.is_array}'
                f'{self.type.name}'
                f'{"!" * self.items_required}'
                f'{"]" * self.is_array}'
                f'{"!" * self.is_required}')

    @staticmethod
    def required(type: Type, **kwargs):
        return TypeSpec(type=type,
                        is_required=True,
                        **kwargs)

    @staticmethod
    def array(type: Type, **kwargs):
        return TypeSpec(type=type,
                        is_array=True,
                        **kwargs)

    @staticmethod
    def from_type_node(node: TypeNode):
        if node.kind == 'non_null_type':
            spec = TypeSpec.from_type_node(node.type)
            spec.is_required = True
            return spec
        if node.kind == 'list_type':
            spec = TypeSpec.from_type_node(node.type)
            return TypeSpec(type=spec.type,
                            is_required=False,
                            is_array=True,
                            items_required=spec.is_required)
        return TypeSpec(type=node.name.value,
                        is_required=False,
                        is_array=False,
                        items_required=False)
