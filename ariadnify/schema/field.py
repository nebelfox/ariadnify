from dataclasses import dataclass, field
from typing import List, Callable, Optional

from graphql import FieldDefinitionNode

from ariadnify.schema import TypeSpec, Arg, Filter, Type, OBJECT
from ariadnify.utils import indent


@dataclass
class Field(Arg):
    args: List[Arg] = field(default_factory=list)
    is_pk: bool = False
    is_unique: bool = False
    is_virtual: bool = False
    is_readonly: bool = False
    filter: Filter = None
    crud_operation: Optional[str] = None

    def _stringify_args(self):
        match len(self.args):
            case 0:
                return ''
            case 1:
                return f'({str(self.args[0])})'
        return indent('(\n' + ',\n'.join(map(str, self.args))) + '\n)'

    def __str__(self):
        return f'{self.description}{self.name}{self._stringify_args()}: {self.type}'

    # def __bool__(self):
    #     return bool(self.args)

    def update_filter_type(self):
        if self.filter is None:
            self.filter = self.type.type.filter

    def update_types(self, type_hook: Callable[[str], Type]):
        super().update_types(type_hook)
        if self.is_pk:
            if self.type.type.kind == OBJECT:
                raise ValueError(
                    f'Field \'{self.name}\' cannot be a primary key '
                    f'because its type is object \'{self.type.type.name}\'')
            if not self.type.is_required:
                raise ValueError(
                    f'Field \'{self.name}\' cannot be a primary key '
                    f'because its type is nullable')
            if self.type.is_array:
                raise ValueError(
                    f'Field \'{self.name}\' cannot be a primary key '
                    f'because it is an array')
        for arg in self.args:
            arg.update_types(type_hook)

    @staticmethod
    def contains_directive(node: FieldDefinitionNode, name: str):
        return any(d.name.value == name for d in node.directives)

    @staticmethod
    def from_field_node(node: FieldDefinitionNode):
        type = TypeSpec.from_type_node(node.type)
        args = list(map(Arg.from_arg_node, node.arguments))
        is_pk = Field.contains_directive(node, 'pk')
        is_unique = Field.contains_directive(node, 'unique')
        is_virtual = len(node.arguments) > 0 \
                     or Field.contains_directive(node, 'virtual')
        is_readonly = is_pk or is_virtual \
                      or Field.contains_directive(node, 'readonly')
        return Field(**Arg.kwargs_from_node(node),
                     type=type,
                     args=args,
                     is_pk=is_pk,
                     is_unique=is_unique,
                     is_virtual=is_virtual,
                     is_readonly=is_readonly,
                     filter=Filter.from_directives(node.directives))
