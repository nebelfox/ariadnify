from dataclasses import dataclass, field
from typing import List, Callable, Literal

from graphql import ObjectTypeDefinitionNode
from inflection import underscore as _

from ariadnify.schema import OBJECT, Field, Type, Filter, Description, TypeSpec
from ariadnify.utils import indent


@dataclass
class Object(Type):
    kind: Literal[OBJECT] = field(init=False, default=OBJECT)
    fields: List[Field] = field(default_factory=list)
    filter: Filter = field(init=False, default=None)
    index: int = None
    hidden_fields: List[Field] = field(default_factory=list)

    def _stringify_fields(self, sparse: bool) -> str:
        return ('\n' * (int(sparse) + 1)).join(str(f) for f in self.fields)

    def stringify(self, sparse: bool = False):
        return (f'{super().__str__()} {{\n'
                f'    {indent(self._stringify_fields(sparse))}\n'
                f'}}')

    def __str__(self) -> str:
        return self.stringify()

    def __bool__(self) -> bool:
        return bool(self.fields)

    def update_filters(self):
        for field in self.fields:
            field.update_filter_type()

    @property
    def filterable_fields(self) -> List[Field]:
        """
        :return: fields with any filter
        """
        return [f for f in self.fields if f.filter]

    @property
    def physical_fields(self) -> List[Field]:
        """
        :return: fields that are not virtual,
            i.e. have 0 arguments and are not annotated via @virtual directive
        """
        return [f for f in self.fields if not f.is_virtual]

    @property
    def physical_attributes(self) -> List[Field]:
        """
        :return: fields that are not readonly,
            i.e. not pk, not virtual, and not annotated via @readonly directive
        """
        return [f for f in self.fields
                if not f.is_readonly]

    @property
    def all_fields(self) -> List[Field]:
        return self.fields + self.hidden_fields

    @property
    def primary_key(self) -> List[Field]:
        return [f for f in self.fields if f.is_pk]

    def update_types(self, type_hook: Callable[[str], Type]) -> None:
        for f in self.fields:
            f.update_types(type_hook)

    def make_backref_field(self, field: Field) -> Field:
        t = field.type
        return Field(
            name=_(self.name),
            description=Description.empty(),
            type=TypeSpec(
                type=self,
                is_required=not t.is_array or t.is_required,
                is_array=not t.is_array,
                items_required=not t.is_array and t.is_required)
        )

    def update_relationships(self) -> None:
        for field in self.fields:
            t = field.type.type
            if t.kind == OBJECT:
                if sum(f.type.type is self for f in t.fields) < 1:
                    t.hidden_fields.append(self.make_backref_field(field))

    @staticmethod
    def from_object_node(node: ObjectTypeDefinitionNode, index: int = -1):
        return Object(**Type.kwargs_from_node(node),
                      fields=list(map(Field.from_field_node, node.fields)),
                      index=index)
