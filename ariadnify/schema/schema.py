from dataclasses import dataclass, field
from typing import List, Dict

from graphql import DocumentNode, parse

from ariadnify.schema import (Object,
                              Field,
                              Filter,
                              VALUE,
                              CONTAINS,
                              RANGE,
                              Scalar, Description)

builtin_scalars = {scalar.name: scalar for scalar in (
    Scalar('String', description=Description.empty(),
           filter=Filter(kind=CONTAINS)),
    Scalar('Int', description=Description.empty(), filter=Filter(kind=RANGE)),
    Scalar('Float', description=Description.empty(), filter=Filter(kind=RANGE)),
    Scalar('Boolean', description=Description.empty(),
           filter=Filter(kind=VALUE)),
    Scalar('ID', description=Description.empty(), filter=None)
)}


@dataclass
class Schema:
    scalar_names: List[str] = field(default_factory=list)
    object_names: List[str] = field(default_factory=list)
    types: Dict[str, Scalar | Object] = field(default_factory=dict)
    query: Object = field(init=False, default_factory=lambda: Object('Query',
                                                                     description=Description.empty()))
    mutation: Object = field(init=False,
                             default_factory=lambda: Object('Mutation',
                                                            description=Description.empty()))

    @property
    def scalars(self) -> List[Scalar]:
        return [self.types[name] for name in self.scalar_names]

    @property
    def objects(self) -> List[Object]:
        return [self.types[name] for name in self.object_names]

    @staticmethod
    def from_document_node(document: DocumentNode):
        schema = Schema(types=builtin_scalars)

        for node in document.definitions:
            if node.kind == 'scalar_type_definition':
                scalar = Scalar.from_scalar_node(node)
                schema.types[scalar.name] = scalar
                schema.scalar_names.append(scalar.name)
            elif node.kind == 'object_type_definition':
                obj = Object.from_object_node(node, len(schema.objects))
                if obj.name == 'Query':
                    schema.query = obj
                elif obj.name == 'Mutation':
                    schema.mutation = obj
                else:
                    schema.types[obj.name] = obj
                    schema.object_names.append(obj.name)

        for obj in (*schema.objects, schema.query, schema.mutation):
            obj.update_types(lambda type_name: schema.types[type_name])
            obj.update_filters()
        for obj in schema.objects:
            obj.update_relationships()

        return schema

    @staticmethod
    def _read_file(path: str) -> str:
        with open(path) as file:
            return file.read()

    @staticmethod
    def from_files(*paths: str):
        source = ''.join(Schema._read_file(path) for path in paths)

        return Schema.from_document_node(parse(source, no_location=True))

    def add_queries(self, *queries: Field):
        self.query.fields.extend(queries)

    def add_mutations(self, *mutations: Field):
        self.mutation.fields.extend(mutations)

    def __str__(self):
        return '\n\n'.join((
            ('schema {\n'
             '  query: Query\n'
             '  mutation: Mutation\n'
             '}'),
            self.query.stringify(sparse=True),
            self.mutation.stringify(sparse=True),
            '\n'.join(str(s) for s in self.scalars),
            '\n\n'.join(str(o) for o in self.objects)
        ))
