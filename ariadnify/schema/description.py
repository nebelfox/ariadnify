from dataclasses import dataclass, field

from graphql import StringValueNode


@dataclass
class Description:
    value: str
    is_block: bool = field(default=False)

    def __bool__(self):
        return self.value is not None

    @staticmethod
    def empty():
        return Description(value=None)

    def __str__(self):
        return self and ((self.is_block
                          and f'"""\n{self.value}\n"""'
                          or f'"{self.value}"') + '\n') or ''

    @staticmethod
    def from_string_value_node(node: StringValueNode):
        return (node is not None
                and Description(value=node.value, is_block=node.block)
                or Description.empty())
