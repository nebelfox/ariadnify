from dataclasses import dataclass
from typing import Literal, Iterable

from graphql import DirectiveNode

from ariadnify.utils import trim_suffix

NONE = 'NONE'
VALUE = 'VALUE'
CONTAINS = 'CONTAINS'
RANGE = 'RANGE'
TIME_RANGE = 'TIME_RANGE'


@dataclass
class Filter:
    kind: Literal[NONE] | Literal[VALUE] | Literal[CONTAINS] | Literal[RANGE] | \
          Literal[TIME_RANGE]
    trim_suffix: str = None
    name: str = None

    @staticmethod
    def from_directives(directives: Iterable[DirectiveNode]):
        filter_dirs = [d for d in directives if d.name.value == 'filter']
        if not filter_dirs:
            return None
        args = {arg.name.value: arg.value.value
                for arg in filter_dirs[0].arguments}
        return Filter(kind=args['kind'],
                      trim_suffix=args.get('trimSuffix'),
                      name=args.get('name'))

    def __bool__(self):
        return self.kind != NONE

    def make_field_name(self, object_field_name: str):
        return (self.name
                or self.trim_suffix and trim_suffix(object_field_name,
                                                    self.trim_suffix)
                or object_field_name)
