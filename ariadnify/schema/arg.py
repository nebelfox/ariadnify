from dataclasses import dataclass
from typing import Callable, Optional

from graphql import ArgumentNode

from ariadnify.schema import Type, TypeSpec, Node, Description


@dataclass
class Arg(Node):
    type: TypeSpec
    ref_type: str | Optional[TypeSpec] = None

    def __str__(self):
        return f'{self.description}{self.name}: {self.type}'

    def update_types(self, type_hook: Callable[[str], Type]):
        self.type.type = type_hook(self.type.type)

    @staticmethod
    def from_arg_node(node: ArgumentNode):
        return Arg(name=node.name.value,
                   description=Description.from_string_value_node(
                       node.description),
                   type=TypeSpec.from_type_node(node.type))
