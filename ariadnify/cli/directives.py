import sys
from os.path import exists

from click import option

from ariadnify.cli import root, flag

DIRECTIVE_DEFINITIONS = '''
enum CrudSpecKind {
    "Generate only specified operations"
    ONLY

    "Don't generate specified operations"
    EXCEPT
}

enum CrudOperation {
    "Query for getting all the instances of the type"
    GET_ALL
    "Query for getting a specific instance of the type by its primary key"
    GET_BY_PK
    "Mutation for creating a new instance of the type"
    CREATE
    "Mutation for updating an existing instance of the type"
    UPDATE
    "Mutation for deleting an existing instance of the type"
    DELETE
}

"Specify what CRUD operations to generate"
directive @crud(
    "How to treat the value of `ops` parameter"
    spec: CrudSpecKind! = EXCEPT,
    "List of operations to either generate or not generate depending of the `spec`"
    ops: [CrudOperation!]! = []
) on OBJECT

"The field is a part of the primary key of the type"
directive @pk on FIELD_DEFINITION

"The value of the field must be unique across all instances of the type"
directive @unique on FIELD_DEFINITION

"The combination of the fields must be unique across all the instances of the type"
directive @uniqueGroup(
    "Names of the fields of the type to constraint"
    fields: [String!]!
) repeatable on OBJECT

"""
The field has a custom resolver.
It means the field is not present on the respective database model and mutation for creating an instance of the type.
"""
directive @virtual on FIELD_DEFINITION

"""
The value of the field is constant after the instance is created,
i.e. the field is not available in the mutation for updating an instance of the type.
"""
directive @readonly on FIELD_DEFINITION

"""Only for fields with array type. Allow adding or removing individual array items via dedicated mutations"""
directive @partiallyUpdatable(
    extend: Boolean! = true,
    remove: Boolean! = true
) on FIELD_DEFINITION


enum FilterKind {
    "No filter. Use it to disable a filter for a specific field which type has a default filter defined"
    NONE

    "Match exact value. The generated filter field is `{field}`"
    VALUE

    "The generated query field is `{field}Contains`"
    CONTAINS

    "Match the value to be within a range. The generated query fields are `{field}GreaterThan` and `{field}LessThan"
    RANGE

    "Match the value to be within a time range. The generated query fields are `{field}After` and `{field}Before`"
    TIME_RANGE
}

"""
Make a field `filterable`, i.e. allow optional filtering in GET_ALL query by this field.

Only the fields with scalar types could be filterable.

All the fields inherit filters from their scalar types
if not overriden or disabled explicitly via @filter(kind = NONE).
"""
directive @filter(
    ""
    kind: FilterKind = VALUE,
    trimSuffix: String,
    name: String
) on SCALAR | FIELD_DEFINITION | ENUM
'''


@root.command(help='Print the Ariadnify directive definitions')
@option('-o', '--output', metavar='PATH',
        help='Write to a file instead of stdout')
@flag('-p', '--prepend', help='Prepend the specified file if it already exists')
@flag('-f', '--force', help='Overwrite the specified file if it already exists '
                            'instead of aborting')
def directives(output: str,
               prepend: bool,
               force: bool) -> None:
    """
    Use this command to initialize a "source" schema file
    with all the available Ariadnify directives.
    """
    if not output:
        return print(DIRECTIVE_DEFINITIONS)
    content: str = DIRECTIVE_DEFINITIONS
    if exists(output):
        if not force:
            sys.exit(f'The specified file `{output}` already exists.'
                     f'Use --force to overwrite the file.'
                     f'Use the --prepend to preserve the file content')

        if prepend:
            with open(output) as file:
                content += '\n\n' + file.read()
    with open(output, 'w') as file:
        file.write(content)
