from ariadnify.cli import root

DEPENDENCIES = (
    'flask',
    'flask-sqlalchemy',
    'flask-migrate',
    'ariadne',
    'psycopg2-binary',
    'gunicorn'
)


@root.command(help='Print generated project dependencies')
def deps():
    """
    List the default dependencies

    Usage example:

    `pipenv install $(ariadnify deps)`
    """
    print(' '.join(DEPENDENCIES))
