from click import group, option


def flag(*args, **kwargs):
    return option(*args, **{'is_flag': True,
                            'show_default': True,
                            'default': False,
                            **kwargs})


@group()
def root():
    """
    A code generator for Ariadne GraphQL services
    """
    pass
