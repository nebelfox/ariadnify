from typing import List

from click import option, argument, Choice

from ariadnify.cli import root
from ariadnify.codegen import make_src_package, make_dockerfile
from ariadnify.schema import Schema


@root.command(help='Generate GraphQL service from schema')
@argument('graphql_files', required=True, nargs=-1)
@option('-o', '--output', metavar='PATH',
        help='Base directory for the project. If not specified, '
             'the file contents would be printed to stdout')
@option('-c', '--create', metavar='PREFIX', default='create', show_default=True,
        help='Prefix for generated CREATE mutations')
@option('-u', '--update', metavar='PREFIX', default='update', show_default=True,
        help='Prefix for generated UPDATE mutations')
@option('-d', '--delete', metavar='PREFIX', default='delete', show_default=True,
        help='Prefix for generated DELETE mutations')
@option('-e', '--explorer', metavar='KIND', default='Apollo',
        show_default=True, type=Choice(('GraphiQL', 'Playground', 'Apollo')),
        help='Kind of explorer the generated service would expose '
             'on /explorer route. Supported explorers are '
             'GraphiQL, Playground and Apollo')
@option('-i', '--dockerfile-base-image', metavar='IMAGE',
        default='python:3.10.9-alpine', show_default=True,
        help='Image to use in FROM clause in generated Dockerfile')
def project(graphql_files: List[str],
            output: str,
            create: str,
            update: str,
            delete: str,
            explorer: str,
            dockerfile_base_image: str) -> None:
    schema = Schema.from_files(*graphql_files)
    src_package = make_src_package(schema, create, update, delete, explorer)
    dockerfile = make_dockerfile(dockerfile_base_image)

    if output:
        src_package.write(output)
        dockerfile.write(output)
