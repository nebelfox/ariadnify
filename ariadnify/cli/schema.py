import sys
from os.path import exists

from click import option, argument

from ariadnify.cli import root, flag
from ariadnify.codegen.gql.schema_graphql import make_schema_graphql_file
from ariadnify.schema import Schema


@root.command(help='Generate a complete GraphQL schema '
                   'with queries and mutations '
                   'from given file with annotated type definitions')
@argument('graphql_files', required=True, nargs=-1)
@option('-o', '--output', metavar='PATH',
        help='Write to a file instead of stdout')
@flag('-p', '--prepend', help='Prepend the specified file if it already exists')
@flag('-f', '--force', help='Overwrite the specified file '
                            'if it already exists instead of aborting')
@option('-c', '--create', metavar='PREFIX', default='create', show_default=True,
        help='Prefix for generated CREATE mutations')
@option('-u', '--update', metavar='PREFIX', default='update', show_default=True,
        help='Prefix for generated UPDATE mutations')
@option('-d', '--delete', metavar='PREFIX', default='delete', show_default=True,
        help='Prefix for generated DELETE mutations')
def schema(graphql_files: str,
           output: str,
           prepend: bool,
           force: bool,
           create: str,
           update: str,
           delete: str) -> None:
    schema = Schema.from_files(*graphql_files)
    content = make_schema_graphql_file(schema,
                                       create,
                                       update,
                                       delete).content

    if not output:
        return print(content)
    if exists(output):
        if not force:
            sys.exit(f'The specified file `{output}` already exists.'
                     f'Use --force to overwrite the file.'
                     f'Use the --prepend to preserve the file content')

        if prepend:
            with open(output) as file:
                content += '\n\n' + file.read()
    with open(output, 'w') as file:
        file.write(content)
