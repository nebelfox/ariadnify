import sys
from os.path import exists

from click import option, argument

from ariadnify.cli import root, flag
from ariadnify.codegen import make_models_package
from ariadnify.schema import Schema


@root.command(help='Generate ORM models for given schema')
@argument('schema_files', required=True, nargs=-1)
@option('-o', '--output', metavar='PATH',
        help='Write each model to a separate file in PATH directory '
             'instead of printing to stdout')
@flag('-f', '--force', help='Overwrite the directory and its content '
                            'if it already exists instead of aborting')
def models(schema_files: str,
           output: str,
           force: bool) -> None:
    schema = Schema.from_files(*schema_files)

    if not output:
        return
    if exists(output):
        if not force:
            sys.exit(f'The specified path `{output}` already exists. '
                     f'Use --force to overwrite.')
    make_models_package(schema).write(output)
