# Ariadnify

Ariadnify is a Python tool that generates code for a web-server with GraphQL API
from GraphQL schema.
Define GraphQL `scalars` and `types` and let the tool write the complete server with CRUD for each entity!

## Stack

The generated project is built upon the following stack:

| Solution                                                                   | Purpose                                                      |
|----------------------------------------------------------------------------|--------------------------------------------------------------|
| [Flask](https://flask.palletsprojects.com/en/2.2.x/)                       | Web framework for Python                                     |
| [SQLAlchemy](https://www.sqlalchemy.org/)                                  | SQL toolkit and ORM for Python                               |
| [Alembic](https://alembic.sqlalchemy.org/en/latest/)                       | Database migration tool for SQLAlchemy                       |
| [Flask SQLAlchemy](https://flask-sqlalchemy.palletsprojects.com/en/3.0.x/) | Flask extension for SQLAlchemy integration                   |
| [Flask Migrate](https://flask-migrate.readthedocs.io/en/latest/)           | Flask extension for Alembic integration                      |
| [Ariadne](https://ariadnegraphql.org/)                                     | Python schema-first library for implementing GraphQL servers |
| [Psycopg](https://www.psycopg.org/)                                        | Python driver for PostgresQL                                 |
| [GraphiQL](https://github.com/graphql/graphiql)                            | Official GraphQL Explorer from GraphQL Foundation            |
| [GraphQL Playground](https://github.com/graphql/graphql-playground)        | Community GraphQL Explorer                                   |

## What You Would Still Have to Write Yourself

- GraphQL `scalar` definitions
- Value parsers and serializers for custom scalars
- GraphQL `type` definitions
- Annotations for defined `scalars` and `types` via [directives](#available-directives)
- Resolvers for `virtual` fields

## What the Ariadnify Can Generate

- [x] Queries for defined `types`
- [x] Resolvers for generated queries
- [x] Mutations for defined `types`
- [x] Resolvers for generated mutations
- [x] ORM models for defined `types`
- [x] `/graphql` endpoint
- [x] `/healthcheck` endpoint
- [x] an endpoint for any of available GraphQL explorers
- [x] Dockerfile with `gunicorn` as a production WSGI server

## How to Use

1. Initialize the type definitions file
   ```shell
   ariadnify directives -o types.graphql
   ```

2. Define scalars and types in the `types.graphql`

3. Annotate the defined scalars and types in `types.graphql` (see [directives](#available-directives) section)

4. Generate the project
   ```sh
   ariadnify project types.graphql -o $PROJECT_PATH
   ```

5. Change current directory to project root

   ```sh
   cd $PROJECT_PATH/src
   ```

6. Install dependencies
   ```sh
   $YOUR_DEPENDENCY_MANAGER_INSTALL_COMMAND $(ariadnify deps)
   ```
   
   Where `$YOUR_DEPENDENCY_MANAGER_INSTALL_COMMAND`
   is a command for installing a list of dependencies
   via your preferred dependency manager.
   E.g. `pip install`, `pipenv install`, `poetry add`, etc.

7. Complete the project by writing code for scalar parsers/serializers and virtual fields resolvers.

8. Set up the database
   ```sh
   export DB_CONN_URL=$YOUR_DB_CONN_URL
   flask db init
   flask db migrate -m "$REVISION_MESSAGE"
   flask db upgrade
   ```

9. Start the server in development mode
   ```sh
   flask run --debug
   ```

## Available Directives

Ariadnify introduces rich customization capabilities 
in declarative manner via GraphQL directives.

### `@crud`

Generate a set of operations for the annotated `type`.

The tool supports the following set of basic CRUD operations,
which are reflected in `enum CrudOperation`:

| Enum Value  | Generates field on | Operation                                            |
|-------------|--------------------|------------------------------------------------------|
| `GET_ALL`   | `Query`            | Read all instances of a type                         |
| `GET_BY_PK` | `Query`            | Read a specific instance of a type by its identifier |
| `CREATE`    | `Mutation`         | Create a new instance of a type                      |
| `UPDATE`    | `Mutation`         | Update an existing instance of a type                |
| `DELETE`    | `Mutation`         | Delete an existing instance of a type                |

If used without arguments, the full list of operations is generated for the annotated type.

### `@pk`

Add the annotated `field` to the primary key of the type.

### `@unique`

Generate a unique constraint for the annotated `field`.

### `@virtual`

Don't generate the corresponding column 
in the database table of the type for the annotated `field`.
Instead, require a custom resolver to be implemented manually.
Use it when the `field` has a custom logic behind it,
e.g. it's value is calculated from other fields,
or it's requested from some external resource, etc.

A virtual `field` doesn't appear in the generated `CREATE` and `UPDATE` mutations.

### `@reaonly`

The value of the annotated `field` cannot be changed after the instance of the type is created.

In other words, a readonly `field` appears in generated `CREATE` mutation, but NOT in `UPDATE` mutation.